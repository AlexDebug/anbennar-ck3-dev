#k_enteben
##d_enteben
###c_enteben
96 = {		#Enteben

    # Misc
    culture =  entebenic
    religion = court_of_adean
    holding = castle_holding

    # History
}
1107 = {

    # Misc
    holding = none

    # History

}
1106 = {

    # Misc
    holding = church_holding

    # History

}
1111 = {

    # Misc
    holding = city_holding

    # History

}

###c_wagonwood
1110 = {	#Wagonwood

    # Misc
    culture =  entebenic
    religion = court_of_adean
	holding = castle_holding

    # History

}
1109 = {

    # Misc
    holding = church_holding

    # History

}
1108 = {

    # Misc
    holding = none

    # History

}

###c_saddleglade
98 = {		#Saddleglade

    # Misc
    culture =  entebenic
    religion = court_of_adean
    holding = castle_holding

    # History
}
1039 = {

    # Misc
    holding = city_holding

    # History

}
1074 = {

    # Misc
    holding = none

    # History

}

##d_great_ording
###high_ording
75 = {		#High Ording

    # Misc
    culture =  entebenic
    religion = court_of_adean 
    holding = castle_holding

    # History
}
1154 = {

    # Misc
    holding = city_holding

    # History

}
1155 = {

    # Misc
    holding = castle_holding

    # History

}
1156 = {

    # Misc
    holding = none

    # History

}
1157 = {

    # Misc
    holding = none

    # History

}

###c_east_ording
92 = {		#East Ording

    # Misc
    culture =  entebenic 
    religion = court_of_adean
    holding = castle_holding

    # History
}
1151 = {

    # Misc
    holding = none

    # History

}
1152 = {

    # Misc
    holding = none

    # History

}
1153 = {

    # Misc
    holding = church_holding

    # History

}

###c_west_ording
107 = {		#West Ording

    # Misc
    culture =  entebenic #Placeholder
    religion = court_of_adean #Placeholder
    holding = castle_holding

    # History
}
1149 = {

    # Misc
    holding = castle_holding

    # History

}
1150 = {

    # Misc
    holding = none

    # History

}

##d_horsegarden
###c_horsegarden
105 = {		#Horsegarden

    # Misc
    culture = entebenic
    religion = court_of_adean
    holding = castle_holding

    # History
}
1118 = {

    # Misc
    holding = church_holding

    # History

}
1119 = {

    # Misc
    holding = none

    # History

}
1120 = {

    # Misc
    holding = city_holding

    # History

}
1121 = {

    # Misc
    holding = none

    # History

}

###c_greatfield
78 = {		#Greatfield

    # Misc
    culture =  entebenic
    religion = court_of_adean
    holding = castle_holding

    # History

}
1122 = {

    # Misc
    holding = castle_holding

    # History

}
1123 = {

    # Misc
    holding = church_holding

    # History

}
1124 = {

    # Misc
    holding = none

    # History

}
1125 = {

    # Misc
    holding = city_holding

    # History

}

##d_crovania
###c_new_adea
82 = {		#New Adea

    # Misc
    culture =  crovanni
    religion = court_of_adean
    holding = castle_holding

    # History
}

###b_adeanscour
1112 = {

    # Misc
    holding = church_holding

    # History

}

###c_crovans_rest
83 = {		#Crovan's Rest

    # Misc
    culture =  crovanni
    religion = court_of_adean
    holding = city_holding

    # History
}
1113 = {    #Haggler's Way from Duke Jorgas' campaign

    # Misc
    culture =  crovanni
    religion = court_of_adean
    holding = castle_holding

    # History

}
1114 = {

    # Misc
    holding = none

    # History

}
1115 = {

    # Misc
    holding = none

    # History

}

###c_mistspear
106 = {		#Mistspear

    # Misc
    culture =  crovanni
    religion = eidoueni
    holding = castle_holding

    # History
}
1116 = {

    # Misc
    holding = none

    # History

}
1117 = {

    # Misc
    holding = none

    # History

}
