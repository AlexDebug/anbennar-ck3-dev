#k_gelkalis
##d_gelkalis
###c_gelkalis
665 = {		#Gelkaris

    # Misc
    culture = gelkar
    religion = old_bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_ash_mis
664 = {		#Ash-mis

    # Misc
    culture = gelkar
    religion = old_bulwari_sun_cult
	holding = castle_holding

    # History
}

##d_gelkarzan
###c_asmangol
655 = {		#Asmangol

    # Misc
    culture = gelkar
    religion = old_bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_toiabazi
656 = {		#Toiabazi

    # Misc
    culture = gelkar
    religion = old_bulwari_sun_cult
	holding = castle_holding

    # History
}

##d_elusadul
###c_elusadul
654 = {		#Elusadul

    # Misc
    culture = gelkar
    religion = old_bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_akurmbag
652 = {		#Akurmbag

    # Misc
    culture = gelkar
    religion = old_bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_yabgis
653 = {		#Yabgis

    # Misc
    culture = gelkar
    religion = old_bulwari_sun_cult
	holding = castle_holding

    # History
}
