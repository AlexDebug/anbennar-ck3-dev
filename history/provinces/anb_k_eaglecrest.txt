#k_eaglecrest
##d_dragonhills
###c_eaglecrest
213 = {		#Eaglecrest

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1420 = {

    # Misc
    holding = church_holding

    # History

}

###c_feycombe
211 = {		#Feycombe

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1409 = {

    # Misc
    holding = none

    # History

}
1410 = {

    # Misc
    holding = none

    # History

}
1412 = {

    # Misc
    holding = city_holding

    # History

}

###c_lodan_crags
150 = {		#Lodan Crags

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1407 = {

    # Misc
    holding = none

    # History

}

##d_fluddhill
###c_fluddhill
336 = {		#Fluddhill

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1424 = {

    # Misc
    holding = none

    # History

}
1425 = {

    # Misc
    holding = church_holding

    # History

}
1426 = {

    # Misc
    holding = city_holding

    # History

}
1427 = {

    # Misc
    holding = none

    # History

}

###c_south_floodmarsh
214 = {		#Floodmark

    # Misc
    culture = vertesker
    religion = cult_of_ara
	holding = castle_holding

    # History
}
1428 = {

    # Misc
    holding = none

    # History

}

###c_north_floodmarsh
215 = {		#Fenstone

    # Misc
    culture = vertesker
    religion = cult_of_ara
	holding = castle_holding

    # History
}
1429 = {

    # Misc
    holding = none

    # History

}

###c_lonevalley
212 = {		#Lonevalley

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1421 = {

    # Misc
    holding = city_holding

    # History

}
1422 = {

    # Misc
    holding = none

    # History

}
