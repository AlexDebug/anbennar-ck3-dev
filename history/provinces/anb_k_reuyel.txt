﻿#k_reuyel
##d_reuyel
###c_reuyel
521 = {		#Re'uyel

	# Misc
	culture = yametsesi
	religion = bulwari_sun_cult
	holding = castle_holding

	# History
}
2836 = {

	# Misc
	holding = church_holding

	# History

}
2837 = {

	# Misc
	holding = city_holding

	# History

}
2838 = {

	# Misc
	holding = none

	# History

}
2839 = {

	# Misc
	holding = none

	# History

}
2851 = {

	# Misc
	holding = city_holding

	# History

}

###c_azkaszelazka
522 = {		#Azka-szel-Azka

	# Misc
	culture = yametsesi
	religion = bulwari_sun_cult
	holding = castle_holding

	# History
}
2840 = {

	# Misc
	holding = church_holding

	# History

}
2841 = {

	# Misc
	holding = city_holding

	# History

}
2842 = {

	# Misc
	holding = none

	# History

}

###c_zorntanas
523 = {		#Zorntanas

	# Misc
	culture = yametsesi
	religion = bulwari_sun_cult
	holding = castle_holding

	# History
}
581 = {		#Yametses

	# Misc
	holding = city_holding

	# History
}
2843 = {

	# Misc
	holding = church_holding

	# History

}

##d_medbahar
###c_kaproya_telen
528 = {		#Kaproya-Telen

	# Misc
	culture = yametsesi
	religion = bulwari_sun_cult
	holding = castle_holding

	# History
}
2844 = {

	# Misc
	holding = church_holding

	# History

}
2845 = {

	# Misc
	holding = none

	# History

}

###c_azka_barzil
529 = {		#Azka Barzil

	# Misc
	culture = yametsesi
	religion = bulwari_sun_cult
	holding = castle_holding

	# History
}
2846 = {

	# Misc
	holding = city_holding

	# History

}
2847 = {

	# Misc
	holding = none

	# History

}

###c_akrad_til
520 = {		#Akrad-til

	# Misc
	culture = yametsesi
	religion = bulwari_sun_cult
	holding = castle_holding

	# History
}
2848 = {

	# Misc
	holding = church_holding

	# History

}
2849 = {

	# Misc
	holding = none

	# History

}
2850 = {

	# Misc
	holding = none

	# History

}

##d_crathanor
###c_lencende
516 = {		#Lencende

	# Misc
	culture = ourdi
	religion = southern_cult_of_castellos
	holding = castle_holding

	# History
}
2823 = {

	# Misc
	holding = church_holding

	# History

}
2824 = {

	# Misc
	holding = city_holding

	# History

}

###c_eloan
517 = {		#Eloan

	# Misc
	culture = yametsesi
	religion = bulwari_sun_cult
	holding = castle_holding

	# History
}
2825 = {

	# Misc
	holding = none

	# History

}

###c_aezbel
518 = {		#Aezbel

	# Misc
	culture = yametsesi
	religion = bulwari_sun_cult
	holding = castle_holding

	# History
}
2826 = {

	# Misc
	holding = church_holding

	# History

}
2827 = {

	# Misc
	holding = none

	# History

}

###c_gemisle
452 = {		#Gemisle

	# Misc
	culture = ilatani
	religion = cult_of_the_dame
	holding = castle_holding

	# History
	1000.1.1 = {
		special_building_slot = gemisle_mines_01
	}
}
2828 = {

	# Misc
	holding = none

	# History

}
2852 = {

	# Misc
	holding = church_holding

	# History

}
2853 = {

	# Misc
	holding = city_holding

	# History

}
2854 = {

	# Misc
	holding = none

	# History

}