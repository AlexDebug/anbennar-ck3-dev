﻿###################
#	Texture sets  #
##################

pattern_textures = {
	name = "kheteratan_decoration_1"
	colormask	= "gfx/portraits/accessory_variations/textures/patterns/kheteratan/kheteratan_decoration_1_masks.dds"
	normal		= "gfx/portraits/accessory_variations/textures/patterns/kheteratan/kheteratan_decoration_1_normal.dds"
	properties	= "gfx/portraits/accessory_variations/textures/patterns/kheteratan/kheteratan_decoration_1_properties.dds"
}

pattern_textures = {
	name = "kheteratan_linen_plain_01"
	colormask	= "gfx/portraits/accessory_variations/textures/patterns/western/western_linen_plain_01_masks.dds"
	normal		= "gfx/portraits/accessory_variations/textures/patterns/western/western_linen_plain_01_normal.dds"
	properties	= "gfx/portraits/accessory_variations/textures/patterns/western/western_linen_plain_01_properties.dds"
}

pattern_textures = {
	name = "kheteratan_linen_fine_plain_01"
	colormask	= "gfx/portraits/accessory_variations/textures/patterns/western/western_linen_plain_01_masks.dds"
	normal		= "gfx/portraits/accessory_variations/textures/patterns/western/western_linen_plain_01_normal.dds"
	properties	= "gfx/portraits/accessory_variations/textures/patterns/western/western_linen_fine_plain_01_properties.dds"
}

pattern_textures = {
	name = "kheteratan_silk_plain_01"
	colormask	= "gfx/portraits/accessory_variations/textures/patterns/western/western_silk_plain_01_masks.dds"
	normal		= "gfx/portraits/accessory_variations/textures/patterns/western/western_silk_plain_01_normal.dds"
	properties	= "gfx/portraits/accessory_variations/textures/patterns/western/western_silk_plain_01_properties.dds"
}


##################
#	Layouts		#
##################

pattern_layout = {
	name = "plain_fabric_full_layout"	
	scale 		= { min = 1	max = 1 }
	rotation 	= { min = 0	max = 0 }	
	offset 		= { x = { min = 0	max = 0 }    y = { min = 0	max = 0 } } 
}
pattern_layout = {
	name = "plain_fabric_layout"	
	scale 		= { min = 0.25	max = 0.25 }
	rotation 	= { min = 0	max = 0 }	
	offset 		= { x = { min = 0	max = 0 }    y = { min = 0	max = 0 } } 
}


###################
#	Variations		#
##################

variation = {
	name = "kheteratan_crown_common"
	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_linen_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_plain_01"	layout = "plain_fabric_layout" }
	}	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_plain_01"	layout = "plain_fabric_layout" }
	}	
	
	color_palette = { weight = 1		texture = "gfx/portraits/accessory_variations/textures/color_palette_kheteratan_crown_common.dds" }	
}

variation = {
	name = "kheteratan_crown_low"
	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_fine_plain_01"	layout = "plain_fabric_layout" }
	}	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "western_silk_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_fine_plain_01"	layout = "plain_fabric_layout" }
	}	
	
	color_palette = { weight = 1		texture = "gfx/portraits/accessory_variations/textures/color_palette_kheteratan_crown_low.dds" }	
}

variation = {
	name = "kheteratan_crown_high"
	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_fine_plain_01"	layout = "plain_fabric_layout" }
	}	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "western_silk_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_fine_plain_01"	layout = "plain_fabric_layout" }
	}	
	pattern = { 
		weight = 1
		r = { textures = "western_silk_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_fine_plain_01"	layout = "plain_fabric_layout" }
	}	
	
	color_palette = { weight = 1		texture = "gfx/portraits/accessory_variations/textures/color_palette_kheteratan_pharaoh_crown.dds" }	
}

variation = {
	name = "kheteratan_decoration_1_common"
	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_decoration_1" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_decoration_1" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_decoration_1" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_decoration_1"	layout = "plain_fabric_layout" }
	}	
	
	color_palette = { weight = 1		texture = "gfx/portraits/accessory_variations/textures/color_palette_kheteratan_decoration_1.dds" }	
}

variation = {
	name = "kheteratan_decoration_1_low"
	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_decoration_1" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_decoration_1" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_decoration_1" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_decoration_1"	layout = "plain_fabric_layout" }
	}	
	
	color_palette = { weight = 1		texture = "gfx/portraits/accessory_variations/textures/color_palette_kheteratan_decoration_1.dds" }	
}

variation = {
	name = "kheteratan_decoration_1_high"
	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_decoration_1" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_decoration_1" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_decoration_1" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_decoration_1"	layout = "plain_fabric_layout" }
	}	
	
	color_palette = { weight = 1		texture = "gfx/portraits/accessory_variations/textures/color_palette_kheteratan_decoration_1.dds" }	
}

variation = {
	name = "kheteratan_clothing_common"
	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_linen_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_plain_01"	layout = "plain_fabric_layout" }
	}	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_plain_01"	layout = "plain_fabric_layout" }
	}	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_plain_01"	layout = "plain_fabric_layout" }
	}
	
	color_palette = { weight = 1		texture = "gfx/portraits/accessory_variations/textures/color_palette_kheteratan_clothes_common.dds" }	
}

variation = {
	name = "kheteratan_clothing_low"
	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_fine_plain_01"	layout = "plain_fabric_layout" }
	}	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_silk_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_fine_plain_01"	layout = "plain_fabric_layout" }
	}	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_silk_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_silk_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_fine_plain_01"	layout = "plain_fabric_layout" }
	}
	
	color_palette = { weight = 1		texture = "gfx/portraits/accessory_variations/textures/color_palette_kheteratan_pharaoh_nobel.dds" }	
}

variation = {
	name = "kheteratan_clothing_high"
	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_fine_plain_01"	layout = "plain_fabric_layout" }
	}	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_silk_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_silk_plain_01"	layout = "plain_fabric_layout" }
	}	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_silk_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_silk_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_silk_plain_01"	layout = "plain_fabric_layout" }
	}
	
	color_palette = { weight = 1		texture = "gfx/portraits/accessory_variations/textures/color_palette_kheteratan_pharaoh_nobel.dds" }	
}

###Female
variation = {
	name = "female_kheteratan_nobility_common_01"
	
	# patterns are sampled using UV-set 2
	# uv-coordinates will be modified with scale, rotation, and offset before the texture is sampled
	# If more than one pattern_layout is provided one will be chosen at random (uniformly, no weights)
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_linen_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_plain_01"	layout = "plain_fabric_layout" }
	}	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_plain_01"	layout = "plain_fabric_layout" }
	}	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_plain_01"	layout = "plain_fabric_layout" }
	}			
	# Color palette textures are 4-wide and N-high.
	# Shaders will chose a row in the texture at random, and each column will be sampled once.
	# First column will be masked by the red channel in the masks, second by the green, etc.
	# If more than one color palette texture is provided one will be chosen at random (uniformly, no weights)
	color_palette = { weight = 1		texture = "gfx/portraits/accessory_variations/textures/color_palette_female_kheteratan_nobility_high_01.dds" }	
}


variation = {
	name = "female_kheteratan_nobility_low_01"
	
	# patterns are sampled using UV-set 2
	# uv-coordinates will be modified with scale, rotation, and offset before the texture is sampled
	# If more than one pattern_layout is provided one will be chosen at random (uniformly, no weights)
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_fine_plain_01"	layout = "plain_fabric_layout" }
	}	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_silk_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_fine_plain_01"	layout = "plain_fabric_layout" }
	}	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		g = { textures = "kheteratan_silk_plain_01" layout = "plain_fabric_layout" }
		b = { textures = "kheteratan_silk_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_linen_fine_plain_01"	layout = "plain_fabric_layout" }
	}			
	# Color palette textures are 4-wide and N-high.
	# Shaders will chose a row in the texture at random, and each column will be sampled once.
	# First column will be masked by the red channel in the masks, second by the green, etc.
	# If more than one color palette texture is provided one will be chosen at random (uniformly, no weights)
	color_palette = { weight = 1		texture = "gfx/portraits/accessory_variations/textures/color_palette_female_kheteratan_nobility_high_01.dds" }	
}


variation = {
	name = "female_kheteratan_nobility_high_01"
	
	# patterns are sampled using UV-set 2
	# uv-coordinates will be modified with scale, rotation, and offset before the texture is sampled
	# If more than one pattern_layout is provided one will be chosen at random (uniformly, no weights)
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_fine_plain_01" layout = "western_silk_brocade_01_layout" }
		g = { textures = "kheteratan_linen_fine_plain_01" layout = "small_trim_layout" }
		b = { textures = "kheteratan_linen_fine_plain_01" layout = "small_trim_layout" }
		a = { textures = "kheteratan_linen_fine_plain_01"	layout = "small_trim_layout" }
	}	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_fine_plain_01" layout = "western_silk_brocade_01_layout" }
		g = { textures = "kheteratan_silk_plain_01" layout = "western_silk_brocade_01_layout" }
		b = { textures = "kheteratan_linen_fine_plain_01" layout = "plain_fabric_layout" }
		a = { textures = "kheteratan_silk_plain_01"	layout = "small_trim_layout" }
	}	
	pattern = { 
		weight = 1
		r = { textures = "kheteratan_linen_fine_plain_01" layout = "western_silk_brocade_01_layout" }
		g = { textures = "kheteratan_silk_plain_01" layout = "western_silk_brocade_01_layout" }
		b = { textures = "kheteratan_silk_plain_01" layout = "indian_silk_brocade_01_layout" }
		a = { textures = "kheteratan_silk_plain_01"	layout = "indian_silk_brocade_01_layout" }
	}			
	# Color palette textures are 4-wide and N-high.
	# Shaders will chose a row in the texture at random, and each column will be sampled once.
	# First column will be masked by the red channel in the masks, second by the green, etc.
	# If more than one color palette texture is provided one will be chosen at random (uniformly, no weights)
	color_palette = { weight = 1		texture = "gfx/portraits/accessory_variations/textures/color_palette_female_kheteratan_nobility_high_01.dds" }	
}