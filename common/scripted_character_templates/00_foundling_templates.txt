﻿
peasant_villager_foundling_character = {
	age = { 5 12 }
	random_traits = yes
	dynasty = none
	culture = scope:county.culture
	faith = scope:county.faith
	gender_female_chance = 50
	#Anbennar
	after_creation = {
		if = {
			limit = {
				NOT = { has_racial_trait = yes }
			}
			assign_racial_trait_effect = yes
		}
	}
}
