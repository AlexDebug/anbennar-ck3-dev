﻿
# just so vanilla building_gfx, clothing_gfx, unit_gfx and coa_gfx don't spam the log, less annoying then commenting EVERY mention

debug_culture = {
	color = { 255 255 255 }

	ethos = ethos_stoic
	heritage = heritage_gerudian
	language = language_common
	martial_custom = martial_custom_male_only
	
	traditions = { }
	
	name_list = name_list_castanorian
	
	coa_gfx = { iberian_christian_clothing_gfx iberian_group_coa_gfx norwegian_coa_gfx iranian_group_coa_gfx magyar_group_coa_gfx mongol_coa_gfx polish_coa_gfx byzantine_group_coa_gfx indo_aryan_group_coa_gfx norse_coa_gfx western_coa_gfx }
	building_gfx = { iberian_building_gfx steppe_building_gfx indian_building_gfx arabic_group_building_gfx berber_group_building_gfx }
	clothing_gfx = { mongol_clothing_gfx iberian_muslim_clothing_gfx indian_clothing_gfx byzantine_clothing_gfx }
	unit_gfx = { iberian_christian_unit_gfx eastern_unit_gfx indian_unit_gfx mongol_unit_gfx norse_unit_gfx iberian_muslim_unit_gfx }

	ethnicities = {
		100 = caucasian_northern_blond
	}
}
