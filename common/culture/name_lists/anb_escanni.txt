﻿name_list_adenner = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {	

		#EU4 - castanorian+elvenized
		Alain Alen Arnold Camir Camor Carlan Clarimond Clothar Colyn Coreg Dalyon Delan Delian Devan Dustyn Edmund 
		Frederic Godryc Henric Humban Humbar Humbert Lain Lan Marcan Petran Peyter Rabard Rican Rycan Ricard 
		Stovan Teagan Toman Tomar Venac Vencan Walter Welyam Wystan
		Caylen Arman

		Adran Albert Aldred Alfons Alfred Alos Alain Ardan Ardor Aril Artorian Artur Aucan Austyn Awen Borian Brandon Brayan Brayden Calas 
		Casten CastI_n Corin Daran Darran Denar Edmund Elran Erel Eren Erlan Evin Galin Gelman Kalas Laurens Marion Maurise Olor Oto Rean Ricain 
		Rion Robin Rogier Sandur Taelar Thal Thiren Tomas Trian Trystan Varian Varion Varil Varilor Vincen Willam Dustin Avery 
		Dominic Vernell Arman Cecill Tristan Alvar Adelar Teagan Andrel Frederic Adrien Adrian Ademar Valeran Valen Emil

		#From Vanilla
		Alaric Clotaire
		Bertold Bertram
		Kaspar Konrad Arnold Arnulf
		Avin Bernhard
		Willem Alwin Barend Egmund Hugo Roland Leuderic
		Ceslin Chlothar
		Osric Sigeric Eadric

		#New
		Garren

	}
	female_names = {
		#EU4
		Adra Alara Alina Amarien Aria Calassa Aucanna Castennia CastI_na Cela Celadora Clarimonde Corina Cora Coraline EilI_s EilI_sabet Erella Erela Galina 
		Galinda Laurenne Lianne Madalein Maria Marianna Mariana Marianne Marien Marina Nara Reanna Thalia Varina Varinna Arabella Margery Cecille 
		Kerstin Alisanne Isobel Isabel Isabella Bella Adeline Amina Willamina Aldresia Sofie Sofia Sybille Constance Eleanore Valence EmilI_e GisE_le Athana

		Auci Bella Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
		Humba Lisban Lisbet Madala Magda Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
		Coraline Cora Corina Cecille Valence Athana

		#From Vanilla
		Bertrada Bertilla Gisela Ida Sophia
		Helene Judith Ida Susanna
		Claudia Christine Ursula
		Alena Fenna Elke 
		Filippa Frida Reynilde Maria Mathilde

	}

	dynasty_of_location_prefix = "dynnp_of"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_white_reachman = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {	#added some north germanic names

		#EU4
		Acromar Adelar Alain Alen Arnold Camir Camor Canrec Carlan Celgal Clothar Coreg Crovan Crovis Corac Corric Dalyon Delan Delian Devac Devan Dustyn Edmund 
		Elecast Gracos Henric Humac Humban Humbar Humbert Lain Lan Madalac Marcan Ottrac Ottran Petran Peyter Rabac Rabard Rican Ricard Rogec 
		Roger Stovac Stovan Teagan Tomac Toman Tomar Tormac Ulric Venac Vencan Walter Welyam Wystan Bellac
		Caylen Armann

		#From Vanilla
		Alaric Caradaig Caltram Aenbecan Domelch Drosten Garalt Maelchon Occo Folcbald Sibod
		Bertold Bertram Boddic Dethard Eggerd Ekbert Ewald Hoger
		Otto Kaspar Konrad Rothard
		Avin Bernhard Osnath Wambald
		Willem Steyn Alwin Barend Egmund Hugo Roland
		
		Ulric Ulfric 
		Anund Alv Arne Arnfinn Arnkjell Arnmod Arnvid Aslak Audun Balder BA_rd BjOErn Eigil Eilif Einar Eirik Erlend Erling Eystein Finn Frej
		Grim Gudbrand Gudleik Gudmund GudrOEd Gunnar Guttorm Haldor Halfdan Halkjell Harald Helge HA_kon HA_vard Inge Ivar Jon KA_re Kolbein Lodin
		Magnus Odd Ogmund Olav Orm Ossor Ottar PAAl Ragnar Ragnvald Rolf Sigurd Skjalg Skofte Skule Svein Sverre SA_mund TorbjOErn Tord Tore
		Torbrand Torfinn Torgeir Torgil
		Tormod Torolf Torstein Trond Tryggve Ulv Vigleik A_le A_mund O_lver O_ystein
	}

	female_names = {
		#EU4
		Auci Bella Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
		Humba Lisban Lisbet Madala Magda Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
		Coraline Cora Corina Cecille Valence Athana

		#From Vanilla
		Gisela Gisela Bertrada Bertilla Bertha Bertrada Gertrud Gisela Ida Sophia
		Helene Hemma Judith Ida Susanna
		Brethoc Claudia Candida Clementia Hildegarde Christine Ursula
		Alena Fenna Elke 
		Filippa Frida Gerda Godila Reynilde Maria Mathilde Oda Odelt Olinde Onna

		Anna Astrid Brigida Cecilia Eldrid Freja Gjertrud Gudrid Gudrun Gunnhild Gyda Gyrid Haldora Homlaug IngebjO_rg Ingjerd Ingrid Jorunn
		Karin Kristina Margrete Maria Martha Ragna Ragnfrid Ragnhild Rannveig Sigrid SnO_frid Sunniva Svanhild Thora Thorborg Thordis A_se A_shild A_sta

	}

	dynasty_of_location_prefix = "dynnp_of"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}



name_list_black_castanorian = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {	#added some north germanic names

		#EU4
		Acromar Adelar Alain Alen Arnold Camir Camor Canrec Carlan Celgal Clothar Coreg Crovan Crovis Corac Corric Dalyon Delan Delian Devac Devan Dustyn Edmund 
		Elecast Gracos Henric Humac Humban Humbar Humbert Lain Lan Madalac Marcan Ottrac Ottran Petran Peyter Rabac Rabard Rican Ricard Rogec 
		Roger Stovac Stovan Teagan Tomac Toman Tomar Tormac Ulric Venac Vencan Walter Welyam Wystan Bellac
		Caylen Armann

		#From Vanilla
		Alaric Caradaig Caltram Aenbecan Domelch Drosten Garalt Maelchon Occo Folcbald Sibod
		Bertold Bertram Boddic Dethard Eggerd Ekbert Ewald Hoger
		Otto Kaspar Konrad Rothard
		Avin Bernhard Osnath Wambald
		Willem Steyn Alwin Barend Egmund Hugo Roland
		
		Ulric Ulfric 
		Anund Alv Arne Arnfinn Arnkjell Arnmod Arnvid Aslak Audun Balder BA_rd BjOErn Eigil Eilif Einar Eirik Erlend Erling Eystein Finn Frej
		Grim Gudbrand Gudleik Gudmund GudrOEd Gunnar Guttorm Haldor Halfdan Halkjell Harald Helge HA_kon HA_vard Inge Ivar Jon KA_re Kolbein Lodin
		Magnus Odd Ogmund Olav Orm Ossor Ottar PAAl Ragnar Ragnvald Rolf Sigurd Skjalg Skofte Skule Svein Sverre SA_mund TorbjOErn Tord Tore
		Torbrand Torfinn Torgeir Torgil
		Tormod Torolf Torstein Trond Tryggve Ulv Vigleik A_le A_mund O_lver O_ystein

		#New
		Garren Garrec Castant 
	}

	female_names = {
		#EU4
		Auci Bella Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
		Humba Lisban Lisbet Madala Magda Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
		Coraline Cora Corina Cecille Valence Athana

		#From Vanilla
		Gisela Gisela Bertrada Bertilla Bertha Bertrada Gertrud Gisela Ida Sophia
		Helene Hemma Judith Ida Susanna
		Brethoc Claudia Candida Clementia Hildegarde Christine Ursula
		Alena Fenna Elke 
		Filippa Frida Gerda Godila Reynilde Maria Mathilde Oda Odelt Olinde Onna

		Agatha Cynehild Ecgwyn Octreda

		Anna Astrid Brigida Cecilia Eldrid Freja Gjertrud Gudrid Gudrun Gunnhild Gyda Gyrid Haldora Homlaug IngebjO_rg Ingjerd Ingrid Jorunn
		Karin Kristina Margrete Maria Martha Ragna Ragnfrid Ragnhild Rannveig Sigrid SnO_frid Sunniva Svanhild Thora Thorborg Thordis A_se A_shild A_sta

	}

	dynasty_of_location_prefix = "dynnp_of"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}



name_list_marcher = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {	#bit of central german names here

		#EU4
		Acromar Adelar Alain Alen Arnold Camir Camor Canrec Carlan Celgal Ciramod Clarimond Clothar Colyn Coreg Crovan Crovis Corac Corric Dalyon Delan Delian Devac Devan Dustyn Edmund 
		Elecast Frederic Godrac Godric Godryc Godwin Gracos Henric Humac Humban Humbar Humbert Lain Lan Madalac Marcan Ottrac Ottran Petran Peyter Rabac Rabard Rican Rycan Ricard Rogec 
		Roger Stovac Stovan Teagan Tomac Toman Tomar Tormac Ulric Venac Vencan Walter Welyam Wystan Bellac
		Caylen Arman Adrien Adrian

		#From Vanilla
		Alaric Caradaig Caltram Aenbecan Domelch Drosten Garalt Maelchon Occo Folcbald Sibod Alarich Clotaire
		Bertold Bertram Boddic Dethard Eggerd Ekbert Ewald Hoger Hulderic Humfried Theoderic Theodoric
		Otto Kaspar Konrad Arnold Arnulf Rothard
		Avin Bernhard Meginulf Osnath Wambald
		Willem Steyn Alwin Barend Egmund Hugo Roland Leuderic
		Ceslin Chararic Charibert Childebert Childeric Chilperic Chlodomer Chlothar Chramnesind Clodio Clodion Clovis Creat

		#New
		Garren Garrec Castant 
	}

	female_names = {
		#EU4
		Auci Bella Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
		Humba Lisban Lisbet Madala Magda Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
		Coraline Cora Corina Cecille Valence Athana

		#From Vanilla
		Gisela Gisela Bertrada Bertilla Bertha Bertrada Gertrud Gisela Ida Sophia
		Helene Hemma Judith Ida Susanna
		Brethoc Claudia Candida Clementia Hildegarde Christine Ursula
		Alena Fenna Elke 
		Filippa Frida Gerda Godila Reynilde Maria Mathilde Oda Odelt Olinde Onna

		Agatha Cynehild Ecgwyn Octreda

	}

	dynasty_of_location_prefix = "dynnp_of"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_castellyrian = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {	

		#EU4 - castanorian+elvenized
		Alain Alen Arnold Camir Camor Carlan Clarimond Clothar Colyn Coreg Dalyon Delan Delian Devan Dustyn Edmund 
		Frederic Godryc Henric Humban Humbar Humbert Lain Lan Marcan Petran Peyter Rabard Rican Rycan Ricard 
		Stovan Teagan Toman Tomar Venac Vencan Walter Welyam Wystan
		Caylen Arman

		Adran Albert Aldred Alfons Alfred Alos Alain Ardan Ardor Aril Artorian Artur Aucan Austyn Awen Borian Brandon Brayan Brayden Calas 
		Casten CastI_n Corin Daran Darran Denar Edmund Elran Erel Eren Erlan Evin Galin Gelman Kalas Laurens Marion Maurise Olor Oto Rean Ricain 
		Rion Robin Rogier Sandur Taelar Thal Thiren Tomas Trian Trystan Varian Varion Varil Varilor Vincen Willam Dustin Avery Lucian 
		Dominic Vernell Arman Cecill Tristan Alvar Adelar Teagan Andrel Frederic Adrien Adrian Ademar Valeran Valen Emil

		#From Vanilla
		Alaric Clotaire
		Bertold Bertram
		Kaspar Konrad Arnold Arnulf
		Avin Bernhard
		Willem Alwin Barend Egmund Hugo Roland Leuderic
		Ceslin Chlothar

		#New
		Garren

	}
	female_names = {
		#EU4
		Adra Alara Alina Amarien Aria Calassa Aucanna Castennia CastI_na Cela Celadora Clarimonde Corina Cora Coraline EilI_s EilI_sabet Erella Erela Galina 
		Galinda Laurenne Lianne Madalein Maria Marianna Mariana Marianne Marien Marina Nara Reanna Thalia Varina Varinna Arabella Margery Cecille 
		Kerstin Alisanne Isobel Isabel Isabella Bella Adeline Amina Willamina Aldresia Sofie Sofia Sybille Constance Eleanore Valence EmilI_e GisE_le Athana

		Auci Bella Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
		Humba Lisban Lisbet Madala Magda Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
		Coraline Cora Corina Cecille Valence Athana

		#From Vanilla
		Bertrada Bertilla Gisela Ida Sophia
		Helene Judith Ida Susanna
		Claudia Christine Ursula
		Alena Fenna Elke 
		Filippa Frida Reynilde Maria Mathilde

	}

	dynasty_of_location_prefix = "dynnp_of"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}


name_list_castellyrian_elvenized = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	male_names = {	

		#EU4 - castanorian+elvenized
		Alain Alen Arnold Camir Camor Carlan Clarimond Clothar Colyn Coreg Dalyon Delan Delian Devan Dustyn Edmund 
		Frederic Godryc Henric Humban Humbar Humbert Lain Lan Marcan Petran Peyter Rabard Rican Rycan Ricard 
		Stovan Teagan Toman Tomar Venac Vencan Walter Welyam Wystan
		Caylen Arman

		Adran Albert Aldred Alfons Alfred Alos Alain Ardan Ardor Aril Artorian Artur Aucan Austyn Awen Borian Brandon Brayan Brayden Calas 
		Casten CastI_n Corin Daran Darran Denar Edmund Elran Erel Eren Erlan Evin Galin Gelman Kalas Laurens Marion Maurise Olor Oto Rean Ricain 
		Rion Robin Rogier Sandur Taelar Thal Thiren Tomas Trian Trystan Varian Varion Varil Varilor Vincen Willam Dustin Avery 
		Dominic Vernell Arman Cecill Tristan Alvar Adelar Teagan Andrel Frederic Adrien Adrian Ademar Valeran Valen Emil

		#From Vanilla
		Alaric Clotaire
		Bertold Bertram
		Kaspar Konrad Arnold Arnulf
		Avin Bernhard
		Willem Alwin Barend Egmund Hugo Roland Leuderic
		Ceslin Chlothar
		Osric Sigeric Eadric

		#New
		Garren

	}
	female_names = {
		#EU4
		Auci Bella Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
		Humba Lisban Lisbet Madala Magda Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
		Coraline Cora Corina Cecille Valence Athana

		Adra Alara Alina Amarien Aria Calassa Aucanna Castennia CastI_na Cela Celadora Clarimonde Corina Cora Coraline EilI_s EilI_sabet Erella Erela Galina 
		Galinda Laurenne Lianne Madalein Maria Marianna Mariana Marianne Marien Marina Nara Reanna Thalia Varina Varinna Arabella Margery Cecille 
		Kerstin Alisanne Isobel Isabel Isabella Bella Adeline Amina Willamina Aldresia Sofie Sofia Sybille Constance Eleanore Valence EmilI_e GisE_le Athana

		#From Vanilla
		Bertrada Bertilla Gisela Ida Sophia
		Helene Judith Ida Susanna
		Claudia Christine Ursula
		Alena Fenna Elke 
		Filippa Frida Reynilde Maria Mathilde

	}

	dynasty_of_location_prefix = "dynnp_of"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}


name_list_castanorian = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {	#bit of central german names here

		#EU4
		Acromar Adelar Adrian Adrien Alain Alen Arman Arnold Bellac Camir Camor Canrec Carlan Caylen Celgal Ciramod Clarimond Clothar
		Colyn Corac Coreg Corric Crovan Crovis Dalyon Delan Delian Devac Devan Dustyn Edmund Elecast Frederic Godrac Godric
		Godryc Godwin Gracos Henric Humac Humban Humbar Humbert Lain Lan Madalac Marcan Ottrac Ottran Petran Peyter Rabac Rabard
		Rican Ricard Rogec Roger Rycan Stovac Stovan Teagan Tomac Toman Tomar Tormac Ulric Venac Vencan Walter Welyam Wystan

		#From Vanilla
		Alaric Caradaig Caltram Aenbecan Domelch Drosten Garalt Maelchon Occo Folcbald Sibod Alarich Clotaire
		Bertold Bertram Boddic Dethard Eggerd Ekbert Ewald Hoger Hulderic Humfried Theoderic Theodoric
		Otto Kaspar Konrad Arnold Arnulf Rothard
		Avin Bernhard Meginulf Osnath Wambald
		Willem Steyn Alwin Barend Egmund Hugo Roland Leuderic
		Ceslin Chararic Charibert Childebert Childeric Chilperic Chlodomer Chlothar Chramnesind Clodio Clodion Clovis Creat

		Osric Morcar Sigeric Eadric

		#New
		Garren Garrec Castant 
	}

	female_names = {
		#EU4
		Auci Bella Clarimonde Clarya Clothilde Constance Coralinde Cora Dalya Etta 
		Humba Lisban Lisbet Madala Magda Matilda Adela Alice Anna Anne Catherine Edith Eleanor Elenor Emma
		Coraline Cora Corina Cecille Valence Athana

		#From Vanilla
		Gisela Gisela Bertrada Bertilla Bertha Bertrada Gertrud Gisela Ida Sophia
		Helene Hemma Judith Ida Susanna
		Brethoc Claudia Candida Clementia Hildegarde Christine Ursula
		Alena Fenna Elke 
		Filippa Frida Gerda Godila Reynilde Maria Mathilde Oda Odelt Olinde Onna

		Agatha Cynehild Ecgwyn Octreda
	}



	dynasty_of_location_prefix = "dynnp_of"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

#Castanite
name_list_old_castanorian = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {

		#Names of Ancient Castanor ALL have a C in there
		
		#From EU4
		Acromar Marcan Arcan Dolmec Melcor Melcon Bracan Dorcan Aucan Canrec Camir Camor Carlan Celgal Clothar Coreg Crovan Crovis Corrac
		Devac Elecast Castor Castorn Godrac Gracos Humac Madalac Ottrac Rabac Rogec Stovac Tomac Tormac 
		Ulric #from Gerudia
		Venac Bellac 

		#From Vanilla
		Castantin Carus Crassus Gordian Alaric Clodion Osric Morcar

		#New
		Suracos Cravoc Cravosian Maldec Tarcan Nerac Ceran Acalos Alacan Cragan Octavos

	}
	female_names = {
		Claranda Alica Catrana Arcana Melcora Marcana Bracana Camira Camora Carlana Celga Cora Macilda Castora Godraca Graca Madaca Ottraca Rogeci Stovaci Camaco
		Venaca Bellaca Clamorna Ecena Clametra Cerene Cerena Zencara Octava Castantina Carusa Clodia Acroma

		Clara Clothilde Brethoc Cothilda
		Claudia Candida Clementia
	}

	dynasty_of_location_prefix = "dynnp_of"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_old_castanorian = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {
		Adrian Alexandru Anghel Aron Bajan Balc Barbat Basarab Bogdan Carol Ciprian Ciubar Claudiu
		Corneliu Costin Damjan Dan Dragos Dumitru Emerik Florin Franjo Gavril Gheorghe Grigore Iacob
		Iancu Ieremia Ilie Ioan Iorghu Iosif Iuga Janos Ladislau Latcu Laurentiu Litovoi Lucian Marin
		Mihai Milos Mircea Moise Nicolaie Petre Pirvu Radovan Radu Sas Seneslav Sergiu Simion
		Stefan Stelian Teodor Tepes Tibor Tihomir Timotei Tudor Valentin Valeriu Vasile Veaceslav
		Victor Vilhelm Vintila Vlad
	}
	female_names = {
		Adelina Adriana Afina Alexandra Alexia Ana Anastasia Angela Arina Clara Dana Ecatarina
		Elena Elisabeta Emilia Eufroysina Felicia Floarea Iacoba Ioana Ionela Irina Iulia Maria Monica
		Natalia Olimpia Paraschiva Petra Roxana Ruxandra Smaranda Sophia Stana Stefana Stefania Teodora
		Tereza Vasilica Violeta Voica Zina
		
	}

	
	dynasty_of_location_prefix = "dynnp_sil"
	
	patronym_suffix_male = "dynnpat_suf_escu"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 40
	mat_grf_name_chance = 10
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 40
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_balmirish = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {
		Adrian Alexandru Anghel Aron Bajan Balc Barbat Basarab Bogdan Carol Ciprian Ciubar Claudiu
		Corneliu Costin Damjan Dan Dragos Dumitru Emerik Florin Franjo Gavril Gheorghe Grigore Iacob
		Iancu Ieremia Ilie Ioan Iorghu Iosif Iuga Janos Ladislau Latcu Laurentiu Litovoi Lucian Marin
		Mihai Milos Mircea Moise Nicolaie Petre Pirvu Radovan Radu Sas Seneslav Sergiu Simion
		Stefan Stelian Teodor Tepes Tibor Tihomir Timotei Tudor Valentin Valeriu Vasile Veaceslav
		Victor Vilhelm Vintila Vlad
	}
	female_names = {
		Adelina Adriana Afina Alexandra Alexia Ana Anastasia Angela Arina Clara Dana Ecatarina
		Elena Elisabeta Emilia Eufroysina Felicia Floarea Iacoba Ioana Ionela Irina Iulia Maria Monica
		Natalia Olimpia Paraschiva Petra Roxana Ruxandra Smaranda Sophia Stana Stefana Stefania Teodora
		Tereza Vasilica Violeta Voica Zina
		
	}

	
	dynasty_of_location_prefix = "dynnp_sil"
	
	patronym_suffix_male = "dynnpat_suf_escu"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 40
	mat_grf_name_chance = 10
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 40
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}