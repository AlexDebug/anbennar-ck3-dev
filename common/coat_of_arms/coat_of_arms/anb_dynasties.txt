@smErmine = 0.27

dynasty_silmuna = { # Silmuna
	pattern = "pattern_solid.dds"	
	color1 = "damerian_black"	
	color2 = "damerian_black"	
	colored_emblem = {
		texture = "ce_anb_elven_ship.dds"
		color1 = "damerian_white"
		color2 = "damerian_blue"
		instance = { position = { 0.5 0.5 }	scale = { 0.8 0.8 }	}
	}
}

dynasty_siloriel = { # Siloriel
	pattern = "pattern_solid.dds"	
	color1 = "damerian_white"	
	color2 = "damerian_white"	
	colored_emblem = {
		texture = "ce_anb_elvenized_rose.dds"
		color1 = "lorentish_red"
		color2 = "lorentish_red_dark"
	}
}

dynasty_divenscourge = { # Divenscourge
	pattern = "pattern_solid.dds"
	color1 = pearlsedge_blue
	color2 = pearlsedge_blue
	colored_emblem = {
		texture = "ce_circle.dds"
		color1 = pearlsedge_pearl
		instance = { position = { 0.25 0.2 } scale = { 0.3 0.3 } }
		instance = { position = { 0.5 0.2 } scale = { 0.3 0.3 } }
		instance = { position = { 0.75 0.2 } scale = { 0.3 0.3 } }
	}
	colored_emblem = {
		texture = "ce_ship_longship.dds"
		color1 = pearlsedge_pearl
		color2 = pearlsedge_pearl
		instance = { position = { 0.5 0.65 } scale = { 0.7 0.7 } }
	}
}

house_pearlman = { # Pearlman
	pattern = "pattern_solid.dds"
	color1 = "pearlsedge_blue"	
	color2 = "pearlsedge_blue"	
	colored_emblem = {
		texture = "ce_circle.dds"
		color1 = "pearlsedge_pearl"
		instance = { position = { 0.5 0.4 }	scale = { 0.8 0.8 }	}
	}
}

house_deranne = { # Deranne
	pattern = "pattern_vertical_split_01.dds"
	color1 = "pearlsedge_blue"	
	color2 = "lorentish_green"
	colored_emblem = {
		texture = "ce_anb_lorentish_circlet.dds"
		color1 = "damerian_white"
		color2 = "damerian_white"
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
	}
	colored_emblem = {
		texture = "ce_anb_elvenized_rose.dds"
		color1 = "derannic_purple"
		color2 = "derannic_pink"
		instance = { position = { 0.5 0.5 }	scale = { 0.7 0.7 }	}
	}
}

dynasty_lorentis = { # Lorentis
	pattern = "pattern_solid.dds"	
	color1 = "lorentish_lilac"
	color2 = "lorentish_lilac"
	colored_emblem = {
		texture = "ce_flower.dds"
		color1 = "lorentish_red_dark"
		color2 = "lorentish_green"
	}
}

house_dameris = { # Dameris
	pattern = "pattern_solid.dds"	
	color1 = "damerian_blue"	
	color2 = "damerian_blue"
	colored_emblem = {
		texture = "ce_anb_old_damerian_moon.dds"
		color1 = "damerian_white"
		color2 = "damerian_white"
		instance = { position = { 0.5 0.5 }	scale = { 0.6 0.6 }	}
	}
}

house_rubentis = { # Rubentis
	pattern = "pattern_checkers_01.dds"
	color1 = lorentish_lilac
	color2 = lorentish_green
	colored_emblem = {
		texture = "ce_flower.dds"
		color1 = lorentish_red_dark
		color2 = lorentish_green
		instance = { position = { 0.30 0.30 } scale = { -0.4 0.4 } }
		instance = { position = { 0.70 0.70 } scale = { -0.4 0.4 } }
	}
	colored_emblem = {
		texture = "ce_tree.dds"
		color1 = lorentish_gold
		color2 = lorentish_gold
		instance = { position = { 0.30 0.70 } scale = { 0.4 0.4 } }
		instance = { position = { 0.70 0.30 } scale = { 0.4 0.4 } }
	}
}

house_rewantis = { # Rewantis
	pattern = "pattern_checkers_01.dds"
	color1 = lorentish_lilac
	color2 = lorentish_red_dark
	colored_emblem = {
		texture = "ce_flower.dds"
		color1 = lorentish_red_dark
		color2 = lorentish_green
		instance = { position = { 0.30 0.30 } scale = { -0.4 0.4 } }
		instance = { position = { 0.70 0.70 } scale = { -0.4 0.4 } }
	}
	colored_emblem = {
		texture = "ce_anb_redglades.dds"
		color1 = white
		instance = { position = { 0.30 0.70 } scale = { 0.4 0.4 } }
		instance = { position = { 0.70 0.30 } scale = { 0.4 0.4 } }
	}
}

house_caradantis = {
	pattern = "pattern_solid.dds"
	color1 = rgb { 5 104 57 }
	colored_emblem = {
		texture = "ce_pile_mask.dds"
		color1 = white
		color2 = white
		instance = { position = { 0.5 0.5 }	scale = { 2 1 }	rotation = 180 }
	}
	colored_emblem = {
		texture = "ce_flower.dds"
		color1 = rgb { 188 48 146 }
		color2 = lorentish_green
		color3 = rgb { 137 33 101 }
		instance = { position = { 0.5 0.4 }	scale = { 0.6 0.6 }	}
	}
}

dynasty_lasean = {
	pattern = "pattern_checkers_01.dds"
	color1 = rgb { 5 104 57 }
	color2 = rgb { 164 29 33 }
	colored_emblem = {
		texture = "ce_pile_mask.dds"
		color1 = white
		instance = { position = { 0.25 0.25 } scale = { 1 0.5 } rotation = 180 }
		instance = { position = { 0.75 0.75 } scale = { 1 0.5 } rotation = 180 }
	}
	colored_emblem = {
		texture = "ce_anb_elvenized_rose.dds"
		color1 = rgb { 188 48 146 }
		color2 = rgb { 137 33 101 }
		instance = { position = { 0.25 0.2 } scale = { 0.35 0.35 } }
		instance = { position = { 0.75 0.7 } scale = { 0.35 0.35 } }
	}
	colored_emblem = {
		texture = "ce_anb_woman.dds"
		color1 = rgb { 238 220 130 }
		color2 = rgb { 238 220 130 }
		color3 = rgb { 238 220 130 }
		instance = { position = { 0.25 0.75 } scale = { -0.5 0.5 } }
		instance = { position = { 0.75 0.25 } scale = { 0.5 0.5 } }
	}
}

dynasty_vis = { # sil Vis
	pattern = "pattern_solid.dds"
	color1 = "damerian_white"	
	color2 = "damerian_white"
	colored_emblem = {
		texture = "ce_anb_ordinary_pale_5.dds"
		color1 = rgb { 183 23 31 }	#red
		color2 = rgb { 183 23 31 }	#red
		instance = { position = { 0.08 0.5 }	scale = { 0.71 1.0 }	}
	}
	colored_emblem = {
		texture = "ce_anb_ordinary_pale_5.dds"
		color1 = rgb { 214 86 32 }	#orange
		color2 = rgb { 214 86 32 }	#orange
		instance = { position = { 0.22 0.5 }	scale = { 0.71 1.0 }	}
	}
	colored_emblem = {
		texture = "ce_anb_ordinary_pale_5.dds"
		color1 = rgb { 216 191 23 }	#yellow
		color2 = rgb { 216 191 23 }	#yellow
		instance = { position = { 0.36 0.5 }	scale = { 0.71 1.0 }	}
	}
	colored_emblem = {
		texture = "ce_anb_ordinary_pale_5.dds"
		color1 = rgb { 10 102 49 }	#green
		color2 = rgb { 10 102 49 }	#green
		instance = { position = { 0.5 0.5 }	scale = { 0.71 1.0 }	}
	}
	colored_emblem = {
		texture = "ce_anb_ordinary_pale_5.dds"
		color1 = "blue"
		color2 = "blue"
		instance = { position = { 0.64 0.5 }	scale = { 0.71 1.0 }	}
	}
	colored_emblem = {
		texture = "ce_anb_ordinary_pale_5.dds"
		color1 = rgb { 77 42 124 }	#dark purple
		color2 = rgb { 77 42 124 }	#dark purple
		instance = { position = { 0.78 0.5 }	scale = { 0.71 1.0 }	}
	}
	colored_emblem = {
		texture = "ce_anb_ordinary_pale_5.dds"
		color1 = rgb { 143 85 163 }	#light purple
		color2 = rgb { 143 85 163 }	#light purple
		instance = { position = { 0.92 0.5 }	scale = { 0.71 1.0 }	}
	}
}

dynasty_roilsardis = {
	pattern = "pattern_solid.dds"
	color1 = "roilsardi_red"	
	color2 = "roilsardi_red"
	colored_emblem = {
		texture = "ce_anb_roilsardi_thorn.dds"
		color1 = "roilsardi_green"
		color2 = "roilsardi_green"
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
	}
}

house_roilsard = {
	pattern = "pattern_solid.dds"
	color1 = "roilsardi_red"	
	color2 = "roilsardi_red"
	colored_emblem = {
		texture = "ce_anb_roilsardi_thorn.dds"
		color1 = "roilsardi_green"
		color2 = "roilsardi_green"
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
	}
}

house_loop = {
	pattern = "pattern_solid.dds"
	color1 = "roilsardi_green"	
	color2 = "roilsardi_green"
	colored_emblem = {
		texture = "ce_anb_roilsardi_thorn.dds"
		color1 = "blue"
		color2 = "blue"
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
	}
}

house_saloren = {
	pattern = "pattern_solid.dds"
	color1 = rgb { 244 132 114 }
	color2 = rgb { 244 132 114 }
	colored_emblem = {
		texture = "ce_anb_roilsardi_thorn.dds"
		color1 = "roilsardi_green"
		color2 = "roilsardi_green"
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
	}
}

house_vivin = {
	pattern = "pattern_solid.dds"
	color1 = "corvurian_red_light"	
	color2 = "corvurian_red_light"
	colored_emblem = {
		texture = "ce_anb_roilsardi_thorn.dds"
		color1 = "black"
		color2 = "black"
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
	}
}

dynasty_gawe = {
	pattern = "pattern_solid.dds"
	color1 = "gawedi_blue"
	color2 = "gawedi_blue"
	colored_emblem = {
		texture = "ce_eagle.dds"
		color1 = rgb { 209 173 103 }
		color2 = "corvurian_red_light"
		color3 = "damerian_white"
		instance = { position = { 0.5 0.5 }	scale = { 1.0 1.0 }	 }
	}
}

house_coldsteel = {
	pattern = "pattern_solid.dds"
	color1 = "grey"
	color2 = "grey"
	colored_emblem = {
		texture = "ce_eagle.dds"
		color1 = rgb { 209 173 103 }
		color2 = "corvurian_red_light"
		color3 = "damerian_white"
		instance = { position = { 0.5 0.5 }	scale = { 1.0 1.0 }	 }
	}
}

dynasty_silebor = {
	pattern = "pattern_solid.dds"
	color1 = "damerian_black"	
	color2 = "damerian_black"
	colored_emblem = {
		texture = "ce_waves_04.dds"
		color1 = rgb { 237 223 131 }
		color2 = rgb { 237 223 131 }
		instance = { position = { 0.5 0.76 }	scale = { 1.5 1.5 }	 }
		instance = { position = { 0.5 1.11 }	scale = { 1.5 1.5 }	 }
	}
	colored_emblem = {
		texture = "ce_waves_04.dds"
		color1 = rgb { 230 179 54 }
		color2 = rgb { 230 179 54 }
		instance = { position = { 0.5 0.85 }	scale = { 1.5 1.5 }	 }
	}
	colored_emblem = {
		texture = "ce_anb_dragon_rampant.dds"
		color1 = rgb { 79 139 159 }
		color2 = "damerian_white"
		color3 = "damerian_white"
		instance = { position = { 0.5 0.45 }	scale = { 0.7 0.7 }	 }
	}
}

dynasty_silistra = {
	pattern = "pattern_solid.dds"
	color1 = "purpure"
	color2 = "purpure"
	colored_emblem = {
		texture = "ce_anb_sea_lion_rampant.dds"
		color1 = "yellow_light"
		color2 = rgb { 34 165 178 }	#turquoise
		color3 = "damerian_white"
		instance = { scale = { -0.8 0.8 }	 }
	}
}

dynasty_ording = { 
	pattern = "pattern_solid.dds"
	color1 = "lorentish_green"
	color2 = "lorentish_green"
	colored_emblem = {
		texture = "ce_unicorn.dds"
		color1 = "damerian_white"
		color2 = "white"
		instance = { position = { 0.5 0.5 }	scale = { 0.8 0.8 }	}
	}
}

dynasty_enteba = {
	pattern = "pattern_solid.dds"
	color1 = rgb { 120 40 40 }
	colored_emblem = {
		texture = "ce_horse_rampant.dds"
		color1 = "damerian_white"
		color2 = "damerian_black"
		instance = { position = { 0.5 0.5 } scale = { 0.9 0.9 } }
	}
}

house_caylentis = {
	pattern = "pattern_checkers_01.dds"
	color1 = "lorentish_lilac"
	color2 = rgb { 120 40 40 }
	colored_emblem = {
		texture = "ce_flower.dds"
		color1 = lorentish_red_dark
		color2 = lorentish_green
		instance = { position = { 0.30 0.30 } scale = { -0.4 0.4 } }
		instance = { position = { 0.70 0.70 } scale = { -0.4 0.4 } }
	}
	colored_emblem = {
		texture = "ce_horse_rampant.dds"
		color1 = damerian_white
		color2 = damerian_black
		instance = { position = { 0.30 0.70 }	scale = { 0.4 0.4 }	}
		instance = { position = { 0.70 0.30 }	scale = { 0.4 0.4 }	}
	}
}

dynasty_caylenoris = {
	pattern = "pattern_checkers_01.dds"
	color1 = "lorentish_lilac"
	color2 = rgb { 120 40 40 }
	colored_emblem = {
		texture = "ce_flower.dds"
		color1 = lorentish_red_dark
		color2 = lorentish_green
		instance = { position = { 0.30 0.30 } scale = { -0.4 0.4 } }
		instance = { position = { 0.70 0.70 } scale = { -0.4 0.4 } }
	}
	colored_emblem = {
		texture = "ce_horse_rampant.dds"
		color1 = damerian_white
		color2 = damerian_black
		instance = { position = { 0.30 0.70 }	scale = { 0.4 0.4 }	}
		instance = { position = { 0.70 0.30 }	scale = { 0.4 0.4 }	}
	}
	colored_emblem = {
		texture = "ce_bendlet.dds"
		color1 = rgb { 57 81 163 }
		color2 = rgb { 57 81 163 }
		instance = { scale = { -1 1 }	}
	}
}

house_kyliande = {
	pattern = "pattern_checkers_01.dds"
	color1 = "lorentish_lilac"
	color2 = rgb { 120 40 40 }
	colored_emblem = {
		texture = "ce_flower.dds"
		color1 = lorentish_red_dark
		color2 = lorentish_green
		instance = { position = { 0.30 0.30 } scale = { -0.4 0.4 } }
		instance = { position = { 0.70 0.70 } scale = { -0.4 0.4 } }
	}
	colored_emblem = {
		texture = "ce_horse_rampant.dds"
		color1 = damerian_white
		color2 = damerian_black
		instance = { position = { 0.30 0.70 }	scale = { 0.4 0.4 }	}
		instance = { position = { 0.70 0.30 }	scale = { 0.4 0.4 }	}
	}
	colored_emblem = {
		texture = "ce_bendlet.dds"
		color1 = rgb { 57 81 163 }
		color2 = rgb { 57 81 163 }
		instance = { scale = { -1 1 }	}
	}
}

dynasty_ventis = {
	pattern = "pattern_shield_01.dds"
	color1 = "lorentish_green"
	color2 = "damerian_white"
	colored_emblem = {
		texture = "ce_anb_grapes.dds"
		color1 = rgb { 217 138 11 }
		color2 = "green"
		instance = { position = { 0.525 0.475 }	scale = { 0.5 0.5 }	}
	}
}

dynasty_casthil = { # Casthil
	pattern = "pattern_vertical_split_01.dds"
	color1 = "yellow"
	color2 = "green"
	colored_emblem = {
		texture = "ce_castle.dds"
		color1 = "white"
		color2 = "yellow"
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
		mask = { 1 }
	}
}

dynasty_balgard = { # Balgard
	pattern = "pattern_bend_01.dds"	
	color1 = "green"	
	color2 = "blue"	
	colored_emblem = {
		texture = "ce_castle.dds"
		color1 = "damerian_white"
		color2 = "blue"
		instance = { position = { 0.5 0.5 }	scale = { 0.75 0.75 }	}
	}
}

dynasty_aldegarde = {
	pattern = "pattern_vertical_split_01.dds"
	color1 = "red"
	color2 = "yellow"
	colored_emblem = {
		texture = "ce_castle.dds"
		color1 = "grey"
		color2 = "grey"
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
	}
}

dynasty_frostwall = {
	pattern = "pattern_solid.dds"
	color1 = rgb { 210 237 246 }	#light blue
	color2 = rgb { 210 237 246 }	#light blue
}

house_alencay = {
	pattern = "pattern_solid.dds"
	color1 = "blue_light"
	color2 = "white"
	colored_emblem = {
		texture = "ce_anb_horn_alt.dds"
		color1 = "yellow_light"
		color2 = "yellow_light"
		instance = { position = { 0.4 0.4 }	scale = { 0.7 0.8 }	}
	}
	colored_emblem = {
		texture = "ce_anb_hoard.dds"
		color1 = "yellow_light"
		color2 = "yellow_light"
		instance = { position = { 0.53 0.6 }	scale = { -0.7 0.4 }	}
	}
}

dynasty_goldeneyes = {
	pattern = "pattern_vertical_split_01.dds"
	color1 = "red"
	color2 = "white"
	colored_emblem = {
		texture = "ce_wolf_head.dds"
		color1 = "grey"
		color2 = "corvurian_red_light"
		color3 = "yellow_light"
		instance = { position = { 0.5 0.5 }	scale = { 1.0 1.0 }	}
	}
}

dynasty_adshaw = { # Adshaw
	pattern = "pattern_solid.dds"
	color1 = "yellow_light"
	color2 = "green_light"
	colored_emblem = {
		texture = "ce_block_02.dds"
		color1 = "brown"
		color2 = "brown"
		instance = { position = { 0.3 0.9 }	scale = { 0.05 0.6 }	}
		instance = { position = { 0.5 0.75 }	scale = { 0.05 0.6 }	}
		instance = { position = { 0.7 0.6 }	scale = { 0.05 0.6 }	}
	}
	colored_emblem = {
		texture = "ce_waves_03.dds"
		color1 = "green_light"
		color2 = "green_light"
		instance = { position = { 1.06 0.85 }	scale = { -3.5 3.5 }	}
		instance = { position = { 0.18 -0.05 }	scale = { -3.5 3.5 }	}
	}
	colored_emblem = {
		texture = "ce_anb_conifer.dds"
		color1 = "green"
		color2 = "green_light"
		instance = { position = { 0.3 0.6 }	scale = { 0.4 0.4 }	}
		instance = { position = { 0.5 0.45 }	scale = { 0.4 0.4 }	}
		instance = { position = { 0.7 0.3 }	scale = { 0.4 0.4 }	}
	}
}

dynasty_silgarion = {
	pattern = "pattern_solid.dds"
	color1 = "ibevar_blue"
	color2 = "ibevar_blue"
	colored_emblem = {
		texture = "ce_anb_snake_staff.dds"
		color1 = "brown_dark"
		color2 = "pink"
		color3 = "black"
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
	}
}

dynasty_bjarnsson = {
	pattern = "pattern_chief.dds"
	color1 = "white"
	color2 = "beige"
	colored_emblem = {
		texture = "ce_anb_bear_head.dds"
		color1 = "brown"
		color2 = "red"
		color3 = "damerian_white"
	}
}

dynasty_estallen = {
	pattern = "pattern_solid.dds"
	color1 = rgb { 141 204 122 }
	color2 = rgb { 141 204 122 }
	colored_emblem = {
		texture = "ce_anb_woman.dds"
		color1 = rgb { 219 160 186 }
		color2 = rgb { 219 160 186 }
		color3 = rgb { 219 160 186 }
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
	}
}

dynasty_leslinpar = {
	pattern = "pattern_checkers_01.dds"
	color1 = "damerian_white"
	color2 = rgb { 64 154 212 }
	colored_emblem = {
		texture = "ce_tree.dds"
		color1 = rgb { 196 171 23 }	#yellow
		color2 = rgb { 196 171 23 }	#yellow
		instance = { position = { 0.33 0.33 }	scale = { 0.37 0.37 }	}
		instance = { position = { 0.67 0.67 }	scale = { 0.37 0.37 }	}
	}
	colored_emblem = {
		texture = "ce_mena_pale.dds"
		color1 = "damerian_white"
		color2 = rgb { 107 107 107 }
		instance = { position = { 0.67 0.37 }	scale = { 0.15 0.2 }	}
	}
	colored_emblem = {
		texture = "ce_fire.dds"
		color1 = "red"
		color2 = "red"
		instance = { position = { 0.67 0.22 }	scale = { 0.05 0.1 }	}
	}
	colored_emblem = {
		texture = "ce_horse_statant.dds"
		color1 = "beige"
		color2 = "beige"
		color3 = "damerian_black"
		instance = { position = { 0.33 0.67 }	scale = { 0.37 0.37 }	}
	}
}

dynasty_wex = {
	pattern = "pattern_solid.dds"
	color1 = "wexonard_purple"
	color2 = "wexonard_purple"
	colored_emblem = {
		texture = "ce_anb_castanorian_citadel.dds"
		color1 = "white"
		color2 = "damerian_black"
		instance = { position = { 0.5 0.45 }	scale = { 0.9 0.9 }	}
	}
}

dynasty_vernid = {
	pattern = "pattern_solid.dds"
	color1 = "verne_beige"
	color2 = "damerian_blue"
	colored_emblem = {
		texture = "ce_wyvern.dds"
		color1 = "verne_wyvern_red"
		color2 = "yellow"
		color3 = "verne_beige"
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
	}
}

dynasty_bronzewing = {
	pattern = "pattern_solid.dds"
	color1 = black
	colored_emblem = {
		texture = "ce_wyvern.dds"
		color1 = rgb { 205 127 50 }
		color2 = rgb { 205 127 50 }
		color3 = black
		instance = { scale = { 0.9 0.9 } }
	}
}

dynasty_tails_end = {
	pattern = "pattern_tricolor_horizontal_01.dds"
	color1 = verne_wyvern_red
	color2 = verne_wyvern_red
	color3 = blue_light
	colored_emblem={
		texture = "ce_waves_04.dds"
		color1 = white
		instance = { position = { 0.5 0.96 } }
		instance = { position = { 0.5 0.73 } }
	}
	colored_emblem = {
		texture = "ce_castle.dds"
		color1 = verne_beige
		color2 = verne_wyvern_red
		instance = {
			position = { 0.5 0.35 }
			scale = { 0.65 0.65 }
		}
	}
}

dynasty_sorncost = {
	pattern = "pattern_solid.dds"
	color1 = "lorentish_gold"
	color2 = "damerian_white"
	colored_emblem = {
		texture = "ce_anb_grapes.dds"
		color1 = "derannic_purple"
		color2 = "green_light"
		instance = { position = { 0.55 0.5 }	scale = { 0.7 0.7 }	}
	}
}

dynasty_eaglecrest = {
	pattern = "pattern_solid.dds"
	color1 = "pearlsedge_blue"
	color2 = "pearlsedge_blue"
	colored_emblem = {
		texture = "ce_ordinary_chevron_5.dds"
		color1 = "grey"
		instance = { position = { 0.5 0.5 }	scale = { 1.0 1.0 }	}
	}
	colored_emblem = {
		texture = "ce_eagle.dds"
		color1 = rgb { 209 173 103 }
		color2 = "corvurian_red_light"
		color3 = "damerian_black"
		instance = { position = { 0.25 0.25 }	scale = { 0.35 0.35 }	}
		instance = { position = { 0.75 0.25 }	scale = { 0.35 0.35 }	}
		instance = { position = { 0.5 0.75 }	scale = { 0.35 0.35 }	}
	}
}

dynasty_redstone = {
	pattern = "pattern_solid.dds"
	color1 = "grey"
	color2 = "grey"
	colored_emblem = {
		texture = "ce_anb_ruby.dds"
		color1 = "lorentish_red"
		color2 = "lorentish_red"
		instance = { position = { 0.5 0.5 }	scale = { 1.0 1.0 }	}
	}
}

dynasty_bisan = {
	pattern = "pattern_solid.dds"
	color1 = rgb { 11 148 68 }	#green
	color2 = rgb { 11 148 68 }	#green
	colored_emblem = {
		texture = "ce_lion_rampant_per_pale.dds"
		color1 = "yellow_light"
		color2 = "corvurian_red_light"
		color3 = "damerian_black"
		instance = { position = { 0.5 0.55 }	scale = { 0.7 0.7 }	}
	}
	colored_emblem = {
		texture = "ce_sword_simple.dds"
		color1 = "grey"
		color2 = rgb { 186 186 196 }	#silver
		instance = { position = { 0.39 0.22 }	scale = { 0.4 0.47 }	rotation = 23 }
	}
}

dynasty_lunetein = {
	pattern = "pattern_solid.dds"
	color1 = "ibevar_blue"
	color2 = "ibevar_blue"
	colored_emblem = {
		texture = "ce_anb_elvenized_moon.dds"
		color1 = rgb { 15 117 188 }	#blue
		color2 = rgb { 15 117 188 }	#blue
		instance = { position = { 0.5 0.5 }	scale = { 0.8 0.8 }	}
	}
}

dynasty_acromis = {
	pattern = "pattern_solid.dds"
	color1 = "beige"
	color2 = "beige"
	colored_emblem = {
		texture = "ce_ragged_staff.dds" #placeholder
		color1 = "damerian_blue"
		color2 = "damerian_blue"
		instance = { position = { 0.25 0.25 }	scale = { 1.0 1.0 }	rotation = 45	}
		instance = { position = { 0.75 0.75 }	scale = { -1.0 1.0 }	rotation = 45	}
		instance = { position = { 0.75 0.25 }	scale = { -1.0 1.0 }	rotation = -45	}
		instance = { position = { 0.25 0.75 }	scale = { 1.0 1.0 }	rotation = -45	}
	}
}
 
dynasty_arannen  = {
	pattern = "pattern_checkers_06.dds"
	color1 = "damerian_white"
	color2 = rgb { 63 204 128 }
	colored_emblem = {
		texture = "ce_checkers_06.dds"
		color1 = "blue_light"
		color2 = "blue"
		instance = { position = { 0.5 0.5 }	scale = { 1.43 1.43 }	}
	}
	colored_emblem = {
		texture = "ce_lion_rampant_per_pale.dds"
		color1 = "damerian_white"
		color2 = "damerian_white"
		color3 = "damerian_black"
		instance = { position = { 0.5 0.5 }	scale = { 0.8 0.8 }	}
	}
}

dynasty_wesdam = {
	pattern = "pattern_shield_01.dds"
	color1 = "lorentish_red_dark"
	color2 = "damerian_white"
	colored_emblem = {
		texture = "ce_anb_elvenized_moon.dds"
		color1 = "damerian_black"
		instance = { position = { 0.5 0.4 }	scale = { 0.4 0.4 }	}
	}
}

dynasty_bennon = {
	pattern = "pattern_solid.dds"
	color1 = rgb { 116 144 175 }
	color2 = rgb { 116 144 175 }
	colored_emblem = {
		texture = "ce_goat.dds"
		color1 = "damerian_white"
		color2 = "damerian_white"
		color3 = "damerian_white"
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
	}
}

dynasty_exwes = {
	pattern = "pattern_solid.dds"
	color1 = "ibevar_blue"
	color2 = "ibevar_blue"
	colored_emblem = {
		texture = "ce_waves_03.dds"
		color1 = "yellow"
		color2 = "yellow"
		instance = { position = { 0.5 0.5 }	scale = { 1.2 1.2 }	}
	}
	colored_emblem = {
		texture = "ce_fish.dds"
		color1 = "ibevar_blue"
		color2 = "ibevar_blue"
		color3 = "black"
		instance = { position = { 0.35 0.35 }	scale = { 0.4 0.4 }	rotation = 45	}
		instance = { position = { 0.65 0.65 }	scale = { 0.4 0.4 }	rotation = 45	}
	}
}

dynasty_tretunis = {
	pattern = "pattern_solid.dds"
	color1 = "orange"	
	color2 = "orange"
	colored_emblem = {
		texture = "ce_rook.dds"
		color1 = "damerian_white"
		color2 = "damerian_white"
		instance = { position = { 0.5 0.5 }	scale = { 0.5 0.8 }	}
	}
}

dynasty_adad = {
	pattern="pattern__solid_designer.dds"
	color1=white
	color2=beige
	color3=red
	colored_emblem = {
		color1 = black
		texture = "ce_pales_undy_04.dds"
		instance = { position = { 0.500000 0.800000 } scale = { 0.333000 0.333000 } }
		instance = { position = { 0.800000 0.500000 } scale = { 0.333000 0.333000 } depth = 1.000000 rotation=90 }
		instance = { position = { 0.200000 0.500000 } scale = { 0.333000 0.333000 } depth = 3.000000 rotation=90 }
		instance = { position = { 0.500000 0.200000 } scale = { 0.333000 0.333000 } depth = 4.000000 }
	}
	
	colored_emblem = {
	color1 = black
	color2 = black
	texture = "ce_doted_roundel_frame.dds"
	instance = { scale = { 0.430000 0.430000 } depth = 2.010000 }
	}
}

dynasty_attalu = {
	pattern="pattern_solid.dds"
	color1=yellow
	color2=beige
	color3=red
	colored_emblem={
		color1=brown
		color2=red
		color3=black
		texture="ce_wyvern.dds"
	instance = { scale = { 0.700000 0.700000 } }
	}	
}

dynasty_ayarzil = {
	pattern = "pattern_solid.dds"
	color1 = yellow_light
	color2 = yellow
	color3 = red
	colored_emblem = {
		color1 = brown_dark
		color2 = damerian_black
		texture = "ce_camel.dds"
		instance = { scale = { 0.700000 0.700000 } }
	}	
}

dynasty_yazkur = {
	pattern = "pattern_solid.dds"
	color1 = yellow_light
	color2 = yellow
	color3 = red
	colored_emblem = {
		color1 = damerian_black
		color2 = white
		color3 = brown_dark
		texture = "ce_wolf_passant_lamb.dds"
		instance = { scale = { 0.700000 0.700000 } }
	}
}

dynasty_zaid = {
	pattern="pattern__solid_designer.dds"
	color1=black
	color2=beige
	color3=red
	colored_emblem = {
		color1 = white
		texture = "ce_pales_undy_04.dds"
		instance = { position = { 0.500000 0.800000 } scale = { 0.333000 0.333000 } }
		instance = { position = { 0.800000 0.500000 } scale = { 0.333000 0.333000 } depth = 1.000000 rotation=90 }
		instance = { position = { 0.200000 0.500000 } scale = { 0.333000 0.333000 } depth = 3.000000 rotation=90 }
		instance = { position = { 0.500000 0.200000 } scale = { 0.333000 0.333000 } depth = 4.000000 }
	}
	
	colored_emblem = {
	color1 = white
	color2 = white
	texture = "ce_doted_roundel_frame.dds"
	instance = { scale = { 0.430000 0.430000 } depth = 2.000000 }
	}
}

dynasty_bulati = {
	pattern = "pattern_tricolor_horizontal_01.dds"
	color1 = black
	color2 = brown_dark
	color3 = yellow_light
	colored_emblem = {
		color1 = lorentish_red
		color2 =verne_beige
		texture = "ce_bow.dds"
	instance = { scale={ 0.700000 0.700000 } }
	}
}

dynasty_ardeth = {
	pattern="pattern_solid.dds"
	color1=yellow_light
	color2=yellow
	color3=red
	colored_emblem={
		color1=damerian_black
		texture="ce_trefoil_eretnid.dds"
	instance = { scale = { 0.700000 0.700000 } }
	}
}

dynasty_surubaz = {
	pattern="pattern_diagonal_split_01.dds"
	color1=white
	color2=white
	color3=black
	color4=black
	colored_emblem={
		color1=black
		color2=black
		texture="ce_eagle_iberia.dds"
	instance = { scale = { 0.900000 0.900000 } }
	}	
}

dynasty_moonbeam = {
	pattern="pattern_horizontal_split_02.dds"
	color1=yellow
	color2=pearlsedge_pearl
	color3=red
	colored_emblem={
		color1=black
		texture="ce_african_koh.dds"
		instance={
			position={ 0.440000 0.520000 }
			scale={ 0.340000 0.390000 }
			depth=5.010000
			rotation=271
		}
	}
	colored_emblem={
		color1=brown_dark
		color2=damerian_black
		texture="ce_pot.dds"
		instance={
			position={ 0.500000 0.330000 }
			scale={ 0.670000 0.670000 }
			depth=7.010000
		}
	}
	colored_emblem={
		color1=pearlsedge_pearl
		texture="ce_square_kaaba.dds"
		instance={
			position={ 0.810000 0.060000 }
			scale={ 1.000000 0.960000 }
			depth=8.010000
		}
		instance={
			position={ 0.290000 0.160000 }
			scale={ 1.000000 0.630000 }
			depth=9.010000
		}
	}
	colored_emblem={
		color1=black
		texture="ce_mena_fess.dds"
		instance={
			position={ 0.530000 0.440000 }
			scale={ 0.210000 0.150000 }
		}
		instance={
			position={ 0.530000 0.600000 }
			scale={ 0.210001 0.160001 }
			depth=1.010000
		}
	}
	colored_emblem={
		color1=pearlsedge_pearl
		color2=yellow
		texture="ce_quadrilobe.dds"
		instance={
			position={ 0.130000 0.340000 }
			scale={ 0.670000 0.670000 }
			depth=10.010000
		}
		instance={
			position={ 0.870000 0.330000 }
			scale={ 0.730000 0.730000 }
			depth=11.010000
		}
	}
	colored_emblem={
		color1=brown_dark
		color2=yellow_light
		texture="ce_escutcheon.dds"
		instance={
			position={ 0.500000 0.590000 }
			scale={ 0.700000 0.700000 }
			depth=6.010000
		}
	}
	colored_emblem={
		color1=yellow
		color2=yellow_light
		texture="ce_barry_06.dds"
		instance={
			position={ 0.540000 0.910000 }
			depth=12.010000
		}
	}
	colored_emblem={
		color1=black
		texture="ce_mena_pale.dds"
		instance={
			position={ 0.460000 0.520000 }
			scale={ 0.170000 0.260000 }
			depth=2.010000
		}
		instance={
			position={ 0.600000 0.520000 }
			scale={ 0.180000 0.260000 }
			depth=3.010000
		}
		instance={
			position={ 0.530000 0.520000 }
			scale={ 0.260000 0.260000 }
			depth=4.010000
		}
	}
}

dynasty_marr = {
	pattern = "pattern_solid.dds"
	color1 = beige
	color2 = beige
	colored_emblem = {
		texture = "ce_griffin_rampant.dds"
		color1 = damerian_white
		color2 = orange
		color3 = damerian_black
		instance = { position = { 0.5 0.5 } scale = { 0.9 0.9 } }
	}
}

dynasty_gallopsway = {
	pattern="pattern__solid_designer.dds"
	color1=black
	color2=black
	colored_emblem={
		color1=yellow_light
		texture="ce_mace.dds"
	}
}

dynasty_natharis = {
	pattern="pattern__solid_designer.dds"
	color1=ibevar_blue
	color2=white
	color3=red
	colored_emblem={
		color1=damerian_blue
		color2=green_light
		texture="ce_waves_03.dds"
		instance={
			scale={ 0.900000 0.900000 }
		}
	}
}

house_ilvan = {
	pattern = "pattern_solid.dds"
	color1 = "lorentish_green"
	colored_emblem = {
		texture = "ce_anb_ilvandet.dds"
		color1 = rgb { 233 224 129 }
	}
}

dynasty_deruwris = {
	pattern = "pattern_solid.dds"
	color1 = "lorentish_green"
	colored_emblem = {
		texture = "ce_anb_ilvandet.dds"
		color1 = rgb { 233 224 129 }
	}
}

dynasty_woodslover = {
	pattern="pattern_solid.dds"
	color1=green_light
	color2=white
	color3=black
	colored_emblem={
		color1=white
		texture="ce_shamrock.dds"
		instance={
			position={ 1.000000 1.000000 }
			scale={ 0.200000 0.200000 }
		}

		instance={
			position={ 0.800000 1.000000 }
			scale={ 0.200000 0.200000 }
			depth=1.010000
		}

		instance={
			position={ 0.600000 1.000000 }
			scale={ 0.200000 0.200000 }
			depth=2.010000
		}

		instance={
			position={ 0.400000 1.000000 }
			scale={ 0.200000 0.200000 }
			depth=3.010000
		}

		instance={
			position={ 0.200000 1.000000 }
			scale={ 0.200000 0.200000 }
			depth=4.010000
		}

		instance={
			position={ 0.000000 1.000000 }
			scale={ 0.200000 0.200000 }
			depth=5.010000
		}

		instance={
			position={ 0.900000 0.800000 }
			scale={ 0.200000 0.200000 }
			depth=6.010000
		}

		instance={
			position={ 0.700000 0.800000 }
			scale={ 0.200000 0.200000 }
			depth=7.010000
		}

		instance={
			position={ 0.500000 0.800000 }
			scale={ 0.200000 0.200000 }
			depth=8.010000
		}

		instance={
			position={ 0.300000 0.800000 }
			scale={ 0.200000 0.200000 }
			depth=9.010000
		}

		instance={
			position={ 0.100000 0.800000 }
			scale={ 0.200000 0.200000 }
			depth=10.010000
		}

		instance={
			position={ 1.000000 0.600000 }
			scale={ 0.200000 0.200000 }
			depth=11.010000
		}

		instance={
			position={ 0.800000 0.600000 }
			scale={ 0.200000 0.200000 }
			depth=12.010000
		}

		instance={
			position={ 0.600000 0.600000 }
			scale={ 0.200000 0.200000 }
			depth=13.010000
		}

		instance={
			position={ 0.400000 0.600000 }
			scale={ 0.200000 0.200000 }
			depth=14.010000
		}

		instance={
			position={ 0.200000 0.600000 }
			scale={ 0.200000 0.200000 }
			depth=15.010000
		}

		instance={
			position={ 0.000000 0.600000 }
			scale={ 0.200000 0.200000 }
			depth=16.010000
		}

		instance={
			position={ 0.900000 0.400000 }
			scale={ 0.200000 0.200000 }
			depth=17.010000
		}

		instance={
			position={ 0.700000 0.400000 }
			scale={ 0.200000 0.200000 }
			depth=18.010000
		}

		instance={
			position={ 0.500000 0.400000 }
			scale={ 0.200000 0.200000 }
			depth=19.010000
		}

		instance={
			position={ 0.300000 0.400000 }
			scale={ 0.200000 0.200000 }
			depth=20.010000
		}

		instance={
			position={ 0.100000 0.400000 }
			scale={ 0.200000 0.200000 }
			depth=21.010000
		}

		instance={
			position={ 1.000000 0.200000 }
			scale={ 0.200000 0.200000 }
			depth=22.010000
		}

		instance={
			position={ 0.800000 0.200000 }
			scale={ 0.200000 0.200000 }
			depth=23.010000
		}

		instance={
			position={ 0.600000 0.200000 }
			scale={ 0.200000 0.200000 }
			depth=24.010000
		}

		instance={
			position={ 0.400000 0.200000 }
			scale={ 0.200000 0.200000 }
			depth=25.010000
		}

		instance={
			position={ 0.200000 0.200000 }
			scale={ 0.200000 0.200000 }
			depth=26.010000
		}

		instance={
			position={ 0.000000 0.200000 }
			scale={ 0.200000 0.200000 }
			depth=27.010000
		}

		instance={
			position={ 0.900000 0.000000 }
			scale={ 0.200000 0.200000 }
			depth=28.010000
		}

		instance={
			position={ 0.700000 0.000000 }
			scale={ 0.200000 0.200000 }
			depth=29.010000
		}

		instance={
			position={ 0.500000 0.000000 }
			scale={ 0.200000 0.200000 }
			depth=30.010000
		}

		instance={
			position={ 0.300000 0.000000 }
			scale={ 0.200000 0.200000 }
			depth=31.010000
		}

		instance={
			position={ 0.100000 0.000000 }
			scale={ 0.200000 0.200000 }
			depth=32.010002
		}

	}

}
