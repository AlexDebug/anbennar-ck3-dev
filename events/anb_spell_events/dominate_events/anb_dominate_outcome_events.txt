﻿#Outcome events for the dominate spell
namespace = anb_dominate_outcome

anb_dominate_outcome.1 = {
	type = character_event
	title = anb_dominate_outcome.1.t
	desc = anb_dominate_outcome.1.d

	theme = witchcraft
	left_portrait = {
		character = scope:owner
		animation = scheme
	}
	right_portrait = {
		character = scope:target
		animation = idle
	}

	#Cast it!
	option = {
		name = anb_dominate_outcome.1.a

		stress_impact = {
			base = major_stress_impact_gain
			magical_affinity_1 = minor_stress_impact_loss
			magical_affinity_2 = medium_stress_impact_loss
			magical_affinity_3 = major_stress_impact_loss
		}
		if = {
			limit = {
				scope:scheme = {
					has_variable = risky_spellbook_taken
				}
			}
			custom_tooltip = anb_enhance_ability_outcome.1.a_risky_spellbook_tt
			stress_impact = {
				base = minor_stress_impact_gain	
			}
		}

		scope:scheme = {
			# Do the Success Roll
			random = {
				chance = scheme_success_chance
				save_scope_value_as = {
					name = scheme_succeeded
					value = yes
				}
				custom_tooltip = anb_dominate_outcome.1.a_success_tt
			}
			
			# Do the Secrecy Roll
			save_scope_value_as = {
			name = discovery_chance
				value = {
					value = 100
					subtract = scheme_secrecy
				}
			}
			random = {
				chance = scope:discovery_chance
				save_scope_value_as = {
					name = scheme_discovered
					value = yes
				}
				custom_tooltip = anb_dominate_outcome.1.a_discovery_tt
			}
			
			if = {
				limit = { exists = scope:scheme_succeeded }
				scheme_owner = { trigger_event = anb_dominate_outcome.2 }
			}
			else = {
				if = {
					limit = { exists = scope:scheme_discovered }
					scheme_owner = { trigger_event = anb_dominate_outcome.3 }
				}
				else = { scheme_owner = { trigger_event = anb_dominate_outcome.4 } }
			}	
		}
	}

	#I need more time!
	option = {
		name = anb_dominate_outcome.1.b
		scope:scheme = { add_scheme_progress = -3 }
		stress_impact = { impatient = minor_stress_impact_gain }
	}

}

#Scheme Success!
anb_dominate_outcome.2 = {
	type = character_event
	title = anb_dominate_outcome.2.t
	desc = anb_dominate_outcome.2.d

	theme = witchcraft
	left_portrait = {
		character = scope:owner
		animation = schadenfreude
	}

	option = {
		name = anb_dominate_outcome.2.a
		dominate_success_effect = yes
		scope:scheme = { end_scheme = yes }
	}
}

#Scheme Discovered!
anb_dominate_outcome.3 = {
	type = character_event
	title = anb_dominate_outcome.3.t
	desc = anb_dominate_outcome.3.d

	theme = witchcraft
	left_portrait = {
		character = scope:owner
		animation = paranoia
	}

	option = {
		name = anb_dominate_outcome.3.a
		dominate_discovery_effect = yes
		scope:scheme = { end_scheme = yes }
	}
}

#Scheme Failed!
anb_dominate_outcome.4 = {
	type = character_event
	title = anb_dominate_outcome.4.t
	desc = anb_dominate_outcome.4.d

	theme = witchcraft
	left_portrait = {
		character = scope:owner
		animation = disbelief
	}
	
	#If at first you don't succeed...
	option = {
		name = anb_dominate_outcome.4.a
		dominate_failure_effect = yes
		scope:scheme = { add_scheme_progress = -6 }
	}

	#It was worth a try.
	option = {
		name = anb_dominate_outcome.4.b
		dominate_failure_effect = yes
		scope:scheme = { end_scheme = yes }
	}

}

#Someone else overtook their mind
anb_dominate_outcome.5 = {
	type = character_event
	title = anb_dominate_outcome.5.t
	desc = anb_dominate_outcome.5.d

	theme = witchcraft
	left_portrait = {
		character = scope:previous_caster
		animation = disbelief
	}
	
	right_portrait = {
		character = scope:target
		animation = stress
	}

	#Disconcerting...
	option = {
		name = anb_dominate_outcome.5.a
	}
}